import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ListItem } from '../model/ListItem';
import { ParamService } from '../service/param.service';

@Component({
  selector: 'app-form3',
  templateUrl: './form3.component.html',
  styleUrls: ['./form3.component.css']
})
export class Form3Component {

    //3. Conectarlos a un API
    //4. Crear un validador personalizado
    //5. Crear un  validador async

  form : FormGroup;
  tiposId : Array<ListItem>;

  constructor(private paramService : ParamService,
              private formBuilder : FormBuilder){
    this.form = this.formBuilder.group({
      email: ["",
              [Validators.required, Validators.minLength(3), Validators.email, Validators.pattern("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")],
              []],
      password: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(10)]],
      check: [true],
      tipoId: ["", [Validators.required]]
    });

    this.tiposId = this.paramService.getTiposId();
  }

  printValues() : void{
    //Acceder directamente al valor
    //alert("Email: " + this.form.value.email);
    //alert("Password: " + this.form.value.password);
    //alert("Check: " + this.form.value.check);
    //alert("Tipo ID: " + this.form.value.tipoId);

    alert("Form valido: " + this.form.valid)

    //Acceder al FormControl
    alert("Email Prsitine: " + this.form.get('email')?.pristine);
    alert("Email Dirty: " + this.form.get('email')?.dirty);
  }

}
