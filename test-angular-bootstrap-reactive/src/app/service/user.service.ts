import { HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../model/User';
import { environment } from 'src/environments/environment';
import { TIPOS_ID } from '../util/TipoId';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  //6. La responsabilidad de invocar y procesar el resultado al servicio
  //7. Interceptor
  //private MAIN_URL : string = "https://gorest.co.in/public/v2/users";
  //private TOKEN : string = "759da2c315f1ab0c74bd630e992d3a67405dc44fc749bfe973d59bd0bc2763e4";

  constructor(private httpClient : HttpClient) { }

  //La responsabilidad de invocar y procesar el resultado a controlador
  getAllUsers() : Observable<Array<User>>{
    return this.httpClient.get<Array<User>>(environment.MAIN_URL);
  }

  getUserById(id : number) : Observable<User>{
    //return this.httpClient.get<User>(this.MAIN_URL.concat("/").concat(id.toString()));
    return this.httpClient.get<User>(`${environment.MAIN_URL}/${id}`);
  }

  getUsersByPage(page : number, perPage : number) : Observable<Array<User>>{
    const params : HttpParams = new HttpParams()
      .set("page", page)
      .set("per_page", perPage);

    /*const params : HttpParams = new HttpParams()
      .append("page", page)
      .append("per_page", perPage);*/

    return this.httpClient.get<Array<User>>(environment.MAIN_URL, {params: params});
  }

  createUser(user : User) : Observable<User>{
    const headers : HttpHeaders = new HttpHeaders()
      .set("Authorization", "Bearer ".concat(environment.TOKEN))
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("MyHeader", "MyValue");

    //return this.httpClient.post<User>(this.MAIN_URL, user, {headers});
    return this.httpClient.post<User>(environment.MAIN_URL, user, {headers: headers});
  }

  createUser1(user : User) : Observable<HttpResponse<User>>{
    const headers : HttpHeaders = new HttpHeaders()
      .set("Authorization", "Bearer ".concat(environment.TOKEN))
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("MyHeader", "MyValue");

    return this.httpClient.post<User>(environment.MAIN_URL, user, {
      headers: headers,
      observe: 'response'
    });
  }

  updateUser(user : User) : Observable<HttpResponse<User>>{
    const headers : HttpHeaders = new HttpHeaders()
      .set("Authorization", "Bearer ".concat(environment.TOKEN))
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("MyHeader", "MyValue");

    //TIPOS_ID.CC;

    //user = new UserS();

    const id : number = user.id || 0;

    //Validar que el ID no sea nulo

    user.id = undefined;

    return this.httpClient.put<User>(`${environment.MAIN_URL}/${id}`, user, {
      headers: headers,
      observe: 'response'
    });
  }
}
