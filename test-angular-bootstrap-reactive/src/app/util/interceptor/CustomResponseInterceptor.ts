import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Route, Router } from "@angular/router";
import { map, Observable } from "rxjs";

@Injectable()
export class CustomResponseIntercetor implements HttpInterceptor{

  constructor(private router : Router){

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map((event : HttpEvent<any>) => {
        if(event instanceof HttpResponse){
          console.log("Interceptor Response: " + JSON.stringify(event.body));
          console.log("Interceptor Response: " + event.status);
          console.log("Interceptor Response: " + event.headers.get("Content-Type"));
          console.log("Interceptor Response: " + event.url);

          if(event.status === 401){
            alert("Redireccionando a Login");
            this.router.navigateByUrl("/login");
          }else{
            const bodyResponse : any = event.body;

            bodyResponse.newField = "MyDato";
            //console.log("--> " + JSON.stringify(bodyResponse));

            const responseClone = event.clone({body: bodyResponse});

            return responseClone;
          }
        }

        return event;
      }));
  }

};

export const CustomResponseInterceptorProvider = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: CustomResponseIntercetor,
    multi: true
  }
];
