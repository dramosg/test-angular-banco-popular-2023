import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ListItem } from '../model/ListItem';
import { ParamService } from '../service/param.service';

@Component({
  selector: 'app-form2',
  templateUrl: './form2.component.html',
  styleUrls: ['./form2.component.css']
})
export class Form2Component {
  form : FormGroup;
  tiposId : Array<ListItem>;

  constructor(private paramService : ParamService){
    this.form = new FormGroup({
      email: new FormControl("",
        [Validators.required, Validators.minLength(3), Validators.email, Validators.pattern("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")],
        []),
      password: new FormControl("",
        [Validators.required, Validators.minLength(3), Validators.maxLength(10)]),
      check: new FormControl(true),
      tipoId: new FormControl("", Validators.required)
    });

    this.tiposId = this.paramService.getTiposId();
  }

  printValues() : void{
    //Acceder directamente al valor
    alert("Email: " + this.form.value.email);
    alert("Password: " + this.form.value.password);
    alert("Check: " + this.form.value.check);
    alert("Tipo ID: " + this.form.value.tipoId);

    //alert("Form valido: " + this.form.valid)

    //Acceder al FormControl
    //alert("Email Prsitine: " + this.form.get('email')?.pristine);
    //alert("Email Dirty: " + this.form.get('email')?.dirty);
  }
}
