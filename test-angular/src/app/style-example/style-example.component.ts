import { Component } from '@angular/core';

@Component({
  selector: 'app-style-example',
  templateUrl: './style-example.component.html',
  styleUrls: ['./style-example.component.css']
})
export class StyleExampleComponent {
  textColor : string = "red";
  textDecoration : string = "none";

  stringStyles : string = "";
  anyStyles : any = {};
  //string: Nombre del estilo CSS a aplicar (por ejemplo: color, background-color)
  //string: Valor (por ejemplo red, blue)
  styles : Record<string, string> = {};

  stringClasses : string = "";
  //string: Nombre de la clase CSS
  //boolean: Si se aplica o no
  classes : Record<string, boolean> = {};

  flag : boolean = true;

  /*cambiarStyle() : void{
    if(this.flag){
      this.textColor = "red";
      this.textDecoration = "none";
    }else{
      this.textColor = "blue";
      this.textDecoration = "underline";
    }

    this.flag = !this.flag;
  }*/

  /*cambiarStyle() : void{
    if(this.flag){
      this.stringStyles = "color: orange; text-decoration: line-through;"
    }else{
      this.stringStyles = "color: green;"
    }

    this.flag = !this.flag;
  }*/

  /*cambiarStyle() : void{
    if(this.flag){
      this.anyStyles = {
        color: 'red',
        'text-decoration': 'line-through'
      }
    }else{
      this.anyStyles = {
        color: 'orange'
      }
    }

    this.flag = !this.flag;
  }*/

  cambiarStyle() : void{
    if(this.flag){
      this.styles = {
        'color': 'red',
        'text-decoration': 'line-through'
      }
    }else{
      this.styles = {
        'color': 'orange'
      }
    }

    this.flag = !this.flag;
  }

  /*cambiarClass() : void{
    this.flag = !this.flag;
  }*/

  /*cambiarClass() : void{
    this.flag = !this.flag;

    if(this.flag){
      this.stringClasses = "myClass1";
    }else{
      this.stringClasses = "myClass2";
    }
  }*/

  /*cambiarClass() : void{
    this.flag = !this.flag;

    if(this.flag){
      this.classes = {
        myClass1: true,
        myClass2: false,
        myClass3: true
      }
    }else{
      this.classes = {
        myClass1: false,
        myClass2: true,
        myClass3: false
      }
    }
  }*/

  cambiarClass() : void{
    this.flag = !this.flag;

    this.classes = {
      myClass1: this.flag,
      myClass2: !this.flag
    }
  }
}
