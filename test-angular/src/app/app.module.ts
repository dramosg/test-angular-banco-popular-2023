import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyComponentComponent } from './my-component/my-component.component';
import { DetailSectionComponent } from './detail-section/detail-section.component';
import { ExternoComponent } from './externo/externo.component';
import { InternoComponent } from './interno/interno.component';
import { Externo2Component } from './externo2/externo2.component';
import { Interno2Component } from './interno2/interno2.component';
import { PersonaMasterComponent } from './persona-master/persona-master.component';
import { PersonaDetailComponent } from './persona-detail/persona-detail.component';
import { FormsModule } from '@angular/forms';
import { IfExampleComponent } from './if-example/if-example.component';
import { IfInnerExampleComponent } from './if-inner-example/if-inner-example.component';
import { ForExampleComponent } from './for-example/for-example.component';
import { SwitchExampleComponent } from './switch-example/switch-example.component';
import { StyleExampleComponent } from './style-example/style-example.component';

@NgModule({
  declarations: [
    AppComponent,
    MyComponentComponent,
    DetailSectionComponent,
    ExternoComponent,
    InternoComponent,
    Externo2Component,
    Interno2Component,
    PersonaMasterComponent,
    PersonaDetailComponent,
    IfExampleComponent,
    IfInnerExampleComponent,
    ForExampleComponent,
    SwitchExampleComponent,
    StyleExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
