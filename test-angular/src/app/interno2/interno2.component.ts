import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-interno2',
  templateUrl: './interno2.component.html',
  styleUrls: ['./interno2.component.css']
})
export class Interno2Component {
  private _firstName : string;
  private _lastName : string;

  constructor(){
    this._firstName = "";
    this._lastName = "";
  }

  @Input()
  set firstName(firstName : string){
    this._firstName = firstName;
  }

  get firstName() : string{
    return this._firstName;
  }

  @Input()
  set lastName(lastName : string){
    this._lastName = lastName;
  }

  get lastName() : string{
    return this._lastName;
  }
}
