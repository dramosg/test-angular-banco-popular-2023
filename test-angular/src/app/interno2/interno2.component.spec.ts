import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Interno2Component } from './interno2.component';

describe('Interno2Component', () => {
  let component: Interno2Component;
  let fixture: ComponentFixture<Interno2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Interno2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Interno2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
