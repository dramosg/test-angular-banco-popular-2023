import { Component } from '@angular/core';
import Persona from '../model/Persona';

@Component({
  selector: 'app-persona-master',
  templateUrl: './persona-master.component.html',
  styleUrls: ['./persona-master.component.css']
})
export class PersonaMasterComponent {
  person : Persona;
  showDetail : boolean;

  constructor(){
    this.person = {
      firstName : "Pedro",
      lastName : "Perez",
      age : 25,
      estudiando : true
    }

    this.showDetail = false;
  }

  showDetailAction() : void{
    this.showDetail = !this.showDetail;
  }

  setCarrera(carrera : string) : void{
    this.person.carrera = carrera;
  }

  setUniversidad(universidad : string) : void{
    this.person.universidad = universidad;
  }
}
