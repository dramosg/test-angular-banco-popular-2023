import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonaMasterComponent } from './persona-master.component';

describe('PersonaMasterComponent', () => {
  let component: PersonaMasterComponent;
  let fixture: ComponentFixture<PersonaMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonaMasterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PersonaMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
