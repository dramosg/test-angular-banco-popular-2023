import { Component } from '@angular/core';

@Component({
  selector: 'app-externo',
  templateUrl: './externo.component.html',
  styleUrls: ['./externo.component.css']
})
export class ExternoComponent {
  primerNombre : string;
  primerApellido : string;
  flag : boolean;

  constructor(){
    this.primerNombre = "";
    this.primerApellido = "";
    this.flag = true;
  }

  cambiarNombre() : void {
    this.primerNombre = (this.flag ? "Cesar" : "Roberto");
    this.primerApellido = (this.flag ? "Cardenas" : "Rodriguez");
    this.flag = !this.flag;
  }

}
