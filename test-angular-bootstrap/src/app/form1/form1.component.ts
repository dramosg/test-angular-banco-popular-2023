import { Component } from '@angular/core';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class Form1Component {
  email : string = "";
  password : string = "";
  check: boolean = true;
  tiposId: string[] = ["CC", "TI", "RC"];
  tipoIdSelect : string = "";

  doSubmit() : void{
    console.log("Email: " + this.email);
    console.log("Password: " + this.password);
    console.log("Check: " + this.check);
    console.log("Tipo ID: " + this.tipoIdSelect);
  }
}
