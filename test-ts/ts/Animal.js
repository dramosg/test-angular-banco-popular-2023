"use strict";
exports.__esModule = true;
var Animal = /** @class */ (function () {
    function Animal(tipo, numeroDientes) {
        this.tipo = tipo;
        this.numeroDientes = numeroDientes;
    }
    Animal.prototype.print = function () {
        console.log("Animal de tipo " + this.tipo + " con " + this.numeroDientes + " dientes");
    };
    Animal.prototype.setTipo = function (tipo) {
        this.tipo = tipo;
    };
    Animal.prototype.getTipo = function () {
        return this.tipo;
    };
    return Animal;
}());
exports["default"] = Animal;
;
/*class Person{

}*/
//export default Animal;
//export {Animal, Person};
