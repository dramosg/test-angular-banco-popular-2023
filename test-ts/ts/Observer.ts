import { from, Observable, of } from "rxjs";

const myArray : number[] = [1, 2, 3];

const myData : Observable<string> = of("Bogotá", "Medellín", "Calí");
const myData1 : Observable<number> = from(myArray);

myData.subscribe(value => {
    console.log("Ciudad: " + value);
});

myData.subscribe(ciudad => {
    console.log("Desde 2 suscriber: " + ciudad);
});

