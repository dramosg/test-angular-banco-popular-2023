export default class Animal{
//class Animal{

    private tipo : string;
    protected numeroDientes : number;
    public birthDate : Date;

    constructor(tipo : string, numeroDientes : number){
        this.tipo = tipo;
        this.numeroDientes = numeroDientes;
    }

    public print() : void{
        console.log("Animal de tipo " + this.tipo + " con " + this.numeroDientes + " dientes");
    }

    public setTipo(tipo : string) : void {
        this.tipo = tipo;
    }

    public getTipo() : string {
        return this.tipo;
    }
};

/*class Person{

}*/

//export default Animal;
//export {Animal, Person};