;
;
var MyClass = /** @class */ (function () {
    function MyClass() {
    }
    MyClass.prototype.print = function () {
        console.log("Imprimiendo desde MyClass");
    };
    MyClass.prototype.sumar = function (num1, num2) {
        return num1 + num2;
    };
    MyClass.prototype.restar = function (num1, num2) {
        return num1 - num2;
    };
    return MyClass;
}());
var myclass = new MyClass();
myclass.print();
console.log(myclass.sumar(5, 2));
console.log(myclass.restar(5, 2));
var myInterface = new MyClass();
myInterface.print();
console.log(myInterface.sumar(10, 2));
//Casting
myInterface.restar(2, 1);
/*const person1 : Person = {
    name: "Roberto",
    age: 50,
    birthDate: new Date()
};

console.log(person1.name);

console.log("Edad antes: " + person1.age);
person1.age = 25;

console.log("Edad después: " + person1.age);

const person2 : Person = {
    name: "Roberto",
    age: 50,
    birthDate: new Date(),
    gender: "M"
};*/ 
