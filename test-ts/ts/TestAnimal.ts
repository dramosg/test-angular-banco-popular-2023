class Animal{

    private tipo : string;
    protected numeroDientes : number;
    public birthDate : Date;

    constructor(tipo : string, numeroDientes : number){
        this.tipo = tipo;
        this.numeroDientes = numeroDientes;
    }

    public print() : void{
        console.log("Animal de tipo " + this.tipo + " con " + this.numeroDientes + " dientes");
    }

    public setTipo(tipo : string) : void {
        this.tipo = tipo;
    }

    public getTipo() : string {
        return this.tipo;
    }
};

class Carnivoro extends Animal{
    private name : string;
    
    constructor(name : string){
        super("Carnivoro", 26);

        this.name = name;

        //super.numeroDientes
    }

    public print() : void {
        super.print();
        console.log("Nombre del animal: " + this.name);
    }

    public comer() : void{
        console.log(this.name + " comiendo");
    }
};

/*let animal : Animal = new Animal("Carnivoro");
animal.print();

animal = new Animal("Herviboro");
//animal.setTipo("Herviboro");
animal.print();
console.log(animal.getTipo());*/

let carnivoro : Carnivoro = new Carnivoro("Perrito");
carnivoro.print();
carnivoro.comer();


let animal : Animal =  new Carnivoro("Gatico");
animal.print();

