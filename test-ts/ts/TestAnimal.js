var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Animal = /** @class */ (function () {
    function Animal(tipo, numeroDientes) {
        this.tipo = tipo;
        this.numeroDientes = numeroDientes;
    }
    Animal.prototype.print = function () {
        console.log("Animal de tipo " + this.tipo + " con " + this.numeroDientes + " dientes");
    };
    Animal.prototype.setTipo = function (tipo) {
        this.tipo = tipo;
    };
    Animal.prototype.getTipo = function () {
        return this.tipo;
    };
    return Animal;
}());
;
var Carnivoro = /** @class */ (function (_super) {
    __extends(Carnivoro, _super);
    function Carnivoro(name) {
        var _this = _super.call(this, "Carnivoro", 26) || this;
        _this.name = name;
        return _this;
        //super.numeroDientes
    }
    Carnivoro.prototype.print = function () {
        _super.prototype.print.call(this);
        console.log("Nombre del animal: " + this.name);
    };
    Carnivoro.prototype.comer = function () {
        console.log(this.name + " comiendo");
    };
    return Carnivoro;
}(Animal));
;
/*let animal : Animal = new Animal("Carnivoro");
animal.print();

animal = new Animal("Herviboro");
//animal.setTipo("Herviboro");
animal.print();
console.log(animal.getTipo());*/
var carnivoro = new Carnivoro("Perrito");
carnivoro.print();
carnivoro.comer();
var animal = new Carnivoro("Gatico");
animal.print();
