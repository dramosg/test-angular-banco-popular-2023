"use strict";
exports.__esModule = true;
var Carnivoro_1 = require("./Carnivoro");
var carnivoro = new Carnivoro_1.Carnivoro("Perrito");
carnivoro.print();
carnivoro.comer();
var animal = new Carnivoro_1.Carnivoro("Gatico");
animal.print();
//Casting
console.log("__________________");
animal.comer();
/*const person1 : Person = {
    name: "Roberto",
    age: 50,
    birthDate: new Date()
};

console.log("Person: " + JSON.stringify(person1));

const book1 : Book = {
    name: "El retorno delm rey",
    pages: 500,
    year: 2000
};

console.log("Book " + JSON.stringify(book1));

printLoQueSea("Cadena");
printLoQueSea(1);
printLoQueSea(Date.now());*/
