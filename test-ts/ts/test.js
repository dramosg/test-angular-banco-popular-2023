/*console.log("Hola mundo desde Typescript");

let cadena : string = "Cadena";
let numero : number;
let valorBoleano : boolean;
let objeto : any;

const PHI : number = 3.1416;

function myFunction(cadena : string) : void{
    console.log(cadena);
};

function myFunction2(num1 : number, num2 : number) : number{
    return num1 + num2;
}

myFunction("My Cadena");
//console.log("Suma: ", myFunction2(5, 3));

let myArray : Array<number> = [];
myArray.push(1);
myArray.push(2);
myArray.push(3);
//myArray.push("Algo");

let myArray2 : number[] = [1, 3, 5];

let varSoportaVarios : string | number;
varSoportaVarios = "Un valor de texto";
//console.log(varSoportaVarios);

varSoportaVarios = 1;
//console.log(varSoportaVarios);

function myFunction3(num1 : number, num2 : number = 1) : number{
    return num1 + num2;
}

console.log(myFunction3(3));
console.log(myFunction3(3, 2));

let mySet : Set<number> = new Set(myArray2);

console.log(mySet);*/
var myNumbers = [1, 3, 5, 7, 6, 1, 10, 15, 6, 2, 10, 15];
var myEvenNumbers = [];
/*myNumbers.forEach(number => {
    if(number % 2 == 0){
        myEvenNumbers.push(number);
    }
});

console.log(myNumbers);
console.log(myEvenNumbers);*/
//myEvenNumbers = myNumbers.filter(number => number % 2 == 0);
//myEvenNumbers = myNumbers.filter(number => number >= 10);
var sumaPares = myNumbers
    .filter(function (number) { return number % 2 == 0; })
    .map(function (number) { return number * 5; })
    .reduce(function (suma, number) { return suma + number; }, 0);
myEvenNumbers = myNumbers.map(function (number) { return number * 2; });
console.log(myEvenNumbers);
/*myNumbers.forEach(number => {
    if(!myEvenNumbers.includes(number)){
        myEvenNumbers.push(number);
    }
});

console.log(myNumbers);
console.log(myEvenNumbers);*/
