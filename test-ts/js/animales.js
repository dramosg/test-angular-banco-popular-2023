class Animal{
    constructor(tipoDeAnimal){
        this.tipoDeAnimal = tipoDeAnimal;
    }

    print(){
        console.log("Estoy en Animal de tipo: " + this.tipoDeAnimal);
    }
}

class Carnivoro extends Animal{
    constructor(nombre){
        super("Carnivoro");

        this.nombre = nombre;
    }

    print(){
        super.print();
        console.log("Nombre del animal: " + this.nombre);
    }

    comer(){
        console.log("Comiendo carne");
    }
}

class Herbivoro extends Animal{

}

var animal = new Animal("Herviboro");
animal.print();

var animal = new Carnivoro("Pepito");
animal.print();
animal.comer();