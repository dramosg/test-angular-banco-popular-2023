import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Opcion1Component } from './opcion1/opcion1.component';
import { Opcion2Component } from './opcion2/opcion2.component';
import { Opcion3Component } from './opcion3/opcion3.component';
import { Page404Component } from './page404/page404.component';

@NgModule({
  declarations: [
    AppComponent,
    Opcion1Component,
    Opcion2Component,
    Opcion3Component,
    Page404Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
