import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Opcion1Component } from './opcion1/opcion1.component';
import { Opcion2Component } from './opcion2/opcion2.component';
import { Opcion3Component } from './opcion3/opcion3.component';
import { Page404Component } from './page404/page404.component';

const routes: Routes = [
  {
    path: 'option1',
    component: Opcion1Component
  },
  {
    path: 'option2',
    component: Opcion2Component
  },
  {
    path: 'option3',
    component: Opcion3Component
  },
  {
    path: '**',
    component: Page404Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
